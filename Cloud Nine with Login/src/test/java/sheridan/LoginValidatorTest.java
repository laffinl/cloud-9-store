package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginCharTypeRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "Luc12345" ) );
	}
	
	@Test
	public void testIsValidLoginCharTypeException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( " " ) );
	}
	
	@Test
	public void testIsValidLoginCharTypeBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "luc123" ) );
	}
	
	@Test
	public void testIsValidLoginCharTypeBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "123luc" ) );
	}
	
	@Test
	public void testIsValidLoginMinCharRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "l2uc123df3f" ) );
	}
	
	@Test
	public void testIsValidLoginMinCharException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "lu" ) );
	}
	
	@Test
	public void testIsValidLoginMinCharBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "luc123" ) );
	}
	
	@Test
	public void testIsValidLoginMinCharBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "luc12" ) );
	}

}
