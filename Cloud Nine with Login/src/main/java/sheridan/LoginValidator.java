package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName(String loginName) {
		return loginName.matches(".*[0-9].*") && loginName.matches(".*[A-Za-z].*")
				&& Character.isLetter(loginName.charAt(0)) && loginName.length() >= 6;
	}
}
